# pychm3
========

`pychm3` is a atom parsing library for Python 3, used for the CHARMMing web application. It is distinct from the Python 2 `pychm` library, and has been redesigned to have a more consistent API as well as a stronger test suite.

## Installation

To install, install via pip or place into your `requirements.txt` file:

```
git+https://git.lobos.nih.gov/charmming/pychm3.git#egg=pychm3
```

## Changes from `pychm`

1. The biggest API change is that `iter_*` methods have been replaced with properties that are full names, respectively. For example, `iter_seg` is not `.segments`. Each property returns a list.
2. The `pychm.lib` folder models have been renamed to their full names, e.g. `pychm.lib.segment`, and the classes contained in the model files have been renamed as well, e.g. `Segment`.
3. `pychm.future.*` has been merged into `pychm.*`.
