"""
This program takes in a bunch of separate PRM and RTF files
and generates a single, CHARMM36-wide, PRM file for use with Q-Chem
The generated CHARMM36.RTF will be used for all future calculations
and the atom types will match exactly with the ones in CHARMM
this way, making it much easier to convert from CHARMM atomtypes
to Q-Chem MM atomtypes, which then allows for further use of these
parameters for QM/MM.
This program makes heavy use of the pychm RTFFile and PRMFile classes
written by FCP. They have been lightly modified for spacing purposes (i.e. making
.Print().split() more viable in parsing the individual parameter lists)
This program can be used to create new Q-Chem PRM files that are compatible
with future versions of the CHARMM forcefield, so long as a different
argument is passed for the path. The path must contain all RTF/PRM files
that are intended to go into the main forcefield file.
~VS 8/18/2014
"""

from pychm3.io.rtf import RTFFile
from pychm3.io.prm import PRMFile
import argparse, sys #for CLI part of the program
import os #for walking
import traceback

parser = argparse.ArgumentParser(description='Takes a folder as input, and reads in all RTF, PRM and STR files inside it, presuming they are in CHARMM format, \
                                and creates a Q-Chem format PRM file (combined topology and parameter file) with this information.')
parser.add_argument('-f', '--filepath', required=True, dest='filepath', nargs=1, action="store", help="File path where the RTF and PRM files are located - this must be a folder, not a single file. This program will check the folder for all RTF/PRM files.")
parser.add_argument('-o', '--output', required=True, dest='out_path', nargs=1, action="store", help="Filename for the output PRM")
args = vars(parser.parse_args(sys.argv[1:]))

def parsePRM(prm,rtf,lines_dict,sort_lines):
    #Fetch all possible PRMs involved
    if "atom" not in prm.prm:
        return lines_dict #I guess you got one of those STR files that have no parameters. Get out of here.
    atomPRM = prm.prm['atom']
    bondPRM = prm.prm['bond'] if "bond" in prm.prm else False #I can think of ion RTF/PRM where this explodes
    anglePRM = prm.prm['angl'] if "angl" in prm.prm else False #urey-brad come from here. details in the angle section of this program.
    dihePRM = prm.prm['dihe'] if "dihe" in prm.prm else False #waters and stuff might not have these
    nbondPRM = prm.prm['nbon'] #use to get vdw. If there is no nbon I'm not sure what to do
    imprPRM = prm.prm['impr'] if "impr" in prm.prm else False
    #even though the vdw goes after the atoms in the PRM, the fundamental problem is that
    #you need the vdw lines to define the atom lines...so the way the file is written is
    #a little backwards in the generation sense. Thus, we go to nbon first.
    vdwDict = {} #associate atom types with vdw indexes. These SHOULD be compartmentalized to an RTF/PRM...
    vdw_line = "vdw %s %s %s\n" #index, sigma, epsilon
    lines_dict['vdw'] = ""
    for nbond in nbondPRM:
        check_nbond = nbond.Print().split()
        atnum = rtf.atom[check_nbond[0].lower()]['number']
        vdwDict[check_nbond[0].lower()] = int(atnum)
        sort_lines['vdw'].append((int(atnum),vdw_line % (atnum,check_nbond[3],check_nbond[2])))
        lines_dict['vdwcount'] += 1
#        lines_dict['vdw'] += vdw_line % (i,check_nbond[3],check_nbond[2]) #may want to multiply Rmin by two? Unsure if this is reasonable
    #now sort vdw by atnum, since this is a preprocessing program speed doesn't really matter a ton
    sort_lines['vdw'] = sorted(sort_lines['vdw'],key=lambda x: x[0])
    lines_dict['vdw'] = ''.join([x[1] for x in sort_lines['vdw']])
    #ok, that's done. Now on to atom lines
    #at this point we have everything from the PRM. Problem is, to get the charge of each atom,
    #we need information from the RTF. To do this without making the RTFFile class balloon up in processing speed,
    #I added a method to it 
    atomCharges = rtf.get_charges() #done, now let's write the atom lines, those are always there
    atom_line = "Atom %s %s %s %s\n"
    for atom in atomPRM:
        check_atom = atom.Print().split()
        atomNum = rtf.atom[check_atom[2]]['number']
        comment = ' '.join(check_atom[4:]) if len(check_atom) > 4 else ""
        #ok, sometimes atoms get defined at the top of the RTF but never get used in any residues
        #this means we don't have any charges we can find. If we have no charges, Q-Chem blows up.
        #Therefore if atomCharges or vdwDict don't have an atom, we ignore it
        if check_atom[2] not in atomCharges:
            print(("Couldn't find " + check_atom[2] + " in any residues in this RTF. It will be ignored. This may be normal - some RTF do not contain the charge information for atoms in MASS lines."))
        else:
            sort_lines['atoms'].append((int(atomNum), (atom_line % (atomNum, atomCharges[check_atom[2]],vdwDict[check_atom[2]],comment)))) #make sure we can sort the atom lines and make them human readable
            lines_dict['atomcount'] += 1 #no reason to skip a number since we're defining these semi-arbitrarily, we just care about vdw lines

    sort_lines['atoms'] = sorted(sort_lines['atoms'],key=lambda x: x[0])
    lines_dict['atoms'] = ''.join([x[1] for x in sort_lines['atoms']])
#        lines_dict['atoms'] += atom_line % (atomNum, atomCharges[check_atom[2]],vdwDict[check_atom[2]],comment)
    #at this point we need to make checks, since it's possible to have a PRM file that just
    #has no bonds, angles, dihe, etc. Think of an ion-only PRM/RTF set.

    if bondPRM:
        bond_line = "Bond %s %s %s %s\n" #source atom, dest atom, force constant, rmin
        for bond in bondPRM:
            check_bond = bond.Print().split()
            atomNums = [rtf.atom[check_bond[i].lower()]['number'] for i in range(0,2)]
            kbond = check_bond[2]
            rmin = check_bond[3] #multiply times 2??
            ftuple = tuple(atomNums) + (kbond,rmin)
            lines_dict['bond'] += bond_line % ftuple

    if anglePRM:
        angle_line = "Angle %s %s %s %s %s\n" #atom 1, 2, 3, force constant, equilibrium angle
        ub_line = "UreyBrad %s %s %s %s %s\n" #atom 1, 2, 3, force constant, s0
        #we do ureybrad and angles at the same time since they come
        #from the same section of the CHARMM PRM file.
        for angle in anglePRM:
            check_angle = angle.Print().split()
            atomNums = [rtf.atom[check_angle[i].lower()]['number'] for i in range(0,3)]
            ktheta = check_angle[3]
            thetazero = check_angle[4]
            try:
                kub = float(check_angle[5]) #if it fails, check_angle[5] is a comment
                szero = float(check_angle[6])
            except:
                kub = False
            if kub:
                ubtuple = tuple(atomNums) + (kub,szero)
                lines_dict['ubrad'] += ub_line % ubtuple
            atuple = tuple(atomNums) + (ktheta,thetazero)
            lines_dict['angle'] += angle_line % atuple

    if dihePRM:
        dihe_line = "Torsion %s %s %s %s %s %s %s %s\n" #atoms 1, 2, 3, 4, force constant, phase angle, multiplicity, comment
        for dihe in dihePRM:
            check_dihe = dihe.Print().split()
            print(check_dihe)
            atomNums = [rtf.atom[check_dihe[i].lower()]['number'] for i in range(0,4)]
            kchi = check_dihe[4]
            mult = check_dihe[5]
            delta = check_dihe[6]
            comment = ' '.join(check_dihe[7:]) if len(check_dihe) > 7 else ""
            #we're skipping the comments probably?
            dtuple = tuple(atomNums) + (kchi,delta,mult,comment)
            lines_dict['dihe'] += dihe_line % dtuple

    if imprPRM:
        impr_line = "Improper %s %s %s %s %s %s %s %s\n" #same as dihedral
        for impr in imprPRM:
            check_impr = impr.Print().split()
            atomNums = [rtf.atom[check_impr[i].lower()]['number'] for i in range(0,4)]
            kchi = check_impr[4]
            mult = check_impr[5]
            delta = check_impr[6]
            comment = ' '.join(check_impr[7:]) if len(check_impr) > 7 else ""
            ituple = tuple(atomNums) + (kchi,delta,mult,comment)
            lines_dict['impr'] += impr_line % ituple
    return lines_dict



#Note: we have a django template for this, but we're not going to use Django
#Django is very finicky and requires a lot of trickery in order to use
#its templating engine without the attached webserver. As such, we shall write
#the lines individually here. Not to file at once, but creating the string
#piece by piece after parsing.

#The below lines are "magic numbers" specific to CHARMM. See the 
#Q-Chem 4.0e (or later) User's Guide for a description
#of their meaning. For CHARMM users, this basically tells Q-Chem
#to use the same kind of parametrization as CHARMM.
prelude_line = "%-24s%s\n"
prelude_lines = prelude_line % ("vdwtype","LENNARD-JONES") + prelude_line % ("radiusrule","ARITHMETIC") +\
        prelude_line % ("radiustype","R-MIN") + prelude_line % ("radiussize","RADIUS") + \
        prelude_line % ("epsilonrule","GEOMETRIC") + prelude_line % ("vdw-14-scale","1.0") + \
        prelude_line % ("chg-14-scale","1.0") + prelude_line % ("dielectric","1.0") + "\n\n\n"

number_lines = "Natom %s\nNvdw %s\n\n" #hold these for later once we actually know how many atom types we have
lines_dict = {'atoms':"// -- ATOMS -- //\n",'vdw':"// -- VAN DER WAALS TYPES -- //\n",\
            'bond':"// -- BONDS -- //\n",'angle':"// -- ANGLES -- //\n",\
            'dihe':"// -- DIHEDRALS/TORSION ANGLES -- //\n",'impr':"// -- IMPROPERS -- //\n",\
            'ubrad':"// -- UREY-BRADLEY TERMS -- //\n",'atomcount':0,'vdwcount':0}
sort_lines = {'atoms':[],'vdw':[]} #these are so the final file becomes human-readable rather than having random order.
#TODO: This is where we go through an os.walk() of the directory
#these two lists will be processed in order. STR files are added once to each list.
rtf_files = []
prm_files = []
fpath = args['filepath'][0]
for root, dirs, files in os.walk(fpath):
    for name in files:
        end = name.split(".")[-1].upper() #get the file extension, we expect users to know what to do here
        if end == "RTF" or end == "STR":
            rtf_files.append(name)
        if end == "PRM" or end == "STR":
            prm_files.append(name)
        #now we have two lists. Let's loop through them.
if len(rtf_files) != len(prm_files): #you always need both
    print("The number of RTF and PRM files in the directory does not match. Make sure each PRM has an accompanying RTF, or that you use properly-formatted stream files.")
    sys.exit(0)
elif len(rtf_files) < 1: #they already match, so they're both empty
    print("You must provide at least one PRM/RTF pair, or STR file.")
    sys.exit(0)
#TODO: Create append method for PRM files so we can have one big one we can read from. 
#for now, jam all the RTFs together since residue names and atom names should never coincide,
#then go PRM file by PRM file downward.
curr_rtf = False
for file in rtf_files:
    newRTF = RTFFile("%s/%s"%(fpath,file))
    if not curr_rtf:
        curr_rtf = newRTF
    else:
        curr_rtf = curr_rtf.append(newRTF)
for file in prm_files:
    try:
        newPRM = PRMFile("%s/%s"%(fpath,file),is_stream=(file.endswith("str"))) #if you don't make your STR file .STR, you're gonna have a bad time
        #we pretend these are separate files, but in reality they're one and the same
        lines_dict = parsePRM(newPRM,curr_rtf,lines_dict,sort_lines) #this does roughly the same as the RTF append method
    except:
        print(file)
        traceback.print_exc()

#now we write the PRM file
#first get the atom length
number_lines = number_lines % (lines_dict['atomcount'],lines_dict['vdwcount'])

whole_prm = "" #put everything in one giant string, so we reduce the number of necessary filewrites
whole_prm = "%s%s%s\n%s\n%s\n%s\n%s\n%s\n%s\n" % (prelude_lines,number_lines,\
                                                lines_dict['atoms'],lines_dict['vdw'],\
                                                lines_dict['bond'],lines_dict['angle'],\
                                                lines_dict['dihe'],lines_dict['impr'],\
                                                lines_dict['ubrad'])

opath = args['out_path'][0]
out_file = open(opath,"w")
out_file.write(whole_prm)
out_file.close()

