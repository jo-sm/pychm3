#!/usr/bin/env python3
"""
Installation script for standalone usage of pychm3.
"""

from distutils.core import setup

setup(
    name='pychm3',
    version='1.0.0',
    description='Basic CHARMMing Python 3 library',
    author='Frank C. Pickard IV, Tim Miller, Vinushka Schalk, Joshua Smock',
    url='git.lobos.nih.gov/charmming/pychm3',
    packages=[
      'pychm3',
      'pychm3.analysis',
      'pychm3.cg', 'pychm3.cg.analysis',
      'pychm3.const',
      'pychm3.io', 'pychm3.io.charmm',
      'pychm3.lib', 'pychm3.lib.toppar',
      'pychm3.utils',
    ],
    package_dir={'pychm3': 'src/pychm3'},
    package_data={'pychm3': ['data/*']},
    requires=['numpy'],
    license='Public Domain',
    classifiers=[
      'Development Status :: 2 - Pre-Alpha',
      'Environment :: Console',
      'Intended Audience :: Science/Research',
      'License :: Public Domain',
      'Operating System :: POSIX :: Linux',
      'Programming Language :: Python'
      'Topic :: Scientific/Engineering :: Molecular Modeling'
    ]
  )
