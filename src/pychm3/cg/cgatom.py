from pychm3.const import alphanum2num
from pychm3.tools import lowerKeys
from pychm3.lib.metaatom import AtomError
from pychm3.lib.baseatom import BaseAtom

class CGAtom(BaseAtom):
    """
    The standard CHARMMing implementation of the coarse-grained
    resolution atom-like object.

    This object does not vary much from the canonical all-atom `Atom`
    object, save for removal of the element Property, and some
    compliance methods.

    Class Attributes
        `_autoInFormat`
        `_sortSegType`
        `_tagMap`
    Parse Definition
        `parse`
    Properties
        `bFactor`
        `segType`
        `weight`
    Public Methods
        `is_ktgo`
        `is_bln`        TODO
        `Print`
    """

    _autoInFormat = 'charmm'
    """
    A string that defines the default input formatting for all class
    instances.
    """

    _sortSegType = {
        'ktgo': 1,
        'bln': 2,
        'bad': 10
    }

    _tagMap = {
        'ktgo': 'atom',
        'bln': 'atom',
        'bad': 'hetatm'
    }

    def __init__(self, text=None, **kwargs):
      super(CGAtom, self).__init__(text, **kwargs)

      self._properties.update({
        'domain': 'auto',
        'structure': None
      })

      del self._properties['b_factor']
      del self._properties['weight']

####################
# Parse Definition #
####################

    def parse(self, in_format):
        """
        Parses _text into the following instance variables:
            `atomNum`
            `atomType`
            `resName`
            `chainid`
            `resid`
            `cart`
            `resIndex`
            `segType`

        Initial values for: `chainid`, `segType`, `resid` and `atomNum`
        are saved as well as `chainid0`, etc.

        Recognized `in_format` values are:
            `"pdborg"`
            `"charmm"`
        """
        if in_format == 'pdborg':
            self.number = self._text[6:11]
            self.type = self._text[12:16]
            self.residue_name = self._text[16:20]
            self.chain_id = self._text[21]
            self.residue_id = self._text[22:26]
            self.cart = (self._text[30:38], self._text[38:46], self._text[46:54])
            self.residue_index = self.residue_id
            self.domain = 'auto'
            self.segment_type = 'auto'
        elif in_format == 'charmm':
            self.number = self._text[6:11]
            self.type = self._text[12:16]
            self.residue_name = self._text[17:21]
            self.chain_id = self._text[72:76]
            self.residue_id = self._text[22:26]
            self.cart = (self._text[30:38], self._text[38:46], self._text[46:54])
            self.residue_index = self.residue_id
            self.domain = 'auto'
            self.segment_type = 'auto'
        else:
            raise AtomError('parse: unknown `in_format`: %s' % in_format)

        # Save initial properties
        self.__initial_props = {
            'number': self.number,
            'chain_id': self.chain_id,
            'segment_type': self.segment_type,
            'residue_id': self.residue_id,
        }

##############
# Properties #
##############

    @property
    def b_factor(self):
        """
        Always returns a float(0), as `bFactor`s are not currently
        implemented in coarse grained models.
        """
        return 0.

    @property
    def domain(self):
        return self.__domain

    @domain.setter
    def domain(self, domain):
        domain = str(domain).strip().lower()

        if domain == 'auto':
            self.__domain = alphanum2num[self.chain_id]
        else:
            self.__domain = domain

    @property
    def prm_string(self):
        return "{}{}{}".format(self.chain_id, self.residue_id, self.type)

    @property
    def segment_type(self):
        return self.__segment_type

    @segment_type.setter
    def segment_type(self, segment_type):
        segment_type = str(segment_type).strip().lower()

        if segment_type == 'auto':
            if self.is_ktgo():
                self.__segment_type = 'ktgo'
            elif self.is_bln():
                self.__segment_type = 'bln'
            else:
                self.__segment_type = 'bad'
        else:
            self.__segment_type = segment_type

    @property
    def weight(self):
        """
        Always returns a float(0), as `weight`s are not currently
        implemented in coarse grained models.
        """
        return 0.

##################
# Public Methods #
##################

    def is_ktgo(self):
        """
        Logic for determining if an atom was constructed using the KT
        Go model, returns a bool.
        """
        return '{}{}'.format(self.chain_id, self.residue_id) == self.residue_name

    def is_bln(self):
        """
        Logic for determining if an atom was constructed using the BLN
        (hydrophobic/hydrophillic/apolar) model, returns a bool.

        TODO
        """
        pass

    def to_str(self, **kwargs):
        """
        Returns a string representation of the Atom object. Formatting
        defaults to the `_autoInFormat` formatting.

        kwargs:
            `outformat`     ["charmm","debug","xdebug","crd","xcrd"]
            `old_chainid`   [False,True]
            `old_segtype`   [False,True]
            `old_resid`     [False,True]
            `old_atomnum`   [False,True]

        >>> thisAtom.Print(outformat="charmm",old_resid=True)
        """
        # Make kwargs case insensitive
        kwargs = lowerKeys(kwargs)
        # kwargs
        out_format = kwargs.get('format', self.__class__._autoInFormat)
        old_chainid = kwargs.get('old_chainid', False)
        old_segType = kwargs.get('old_segtype', False)
        old_resid = kwargs.get('old_resid', False)
        old_atomNum = kwargs.get('old_atomnum', False)
        # Localize variables
        x, y, z = self.cart

        if old_chainid:
            chain_id = self.chain_id_init
        else:
            chain_id = self.chain_id
        if old_segType:
            segment_type = self.segment_type_init
        else:
            segment_type = self.segment_type
        if old_resid:
            residue_id = self.residue_id_init
        else:
            residue_id = self.residue_id
        if old_atomNum:
            atom_number = self.number_init
        else:
            atom_number = self.number

        # Set formatting
        if out_format == 'charmm':
            output = '%-6s%5i %4s %4s %4i    %8.3f%8.3f%8.3f%6.2f%6.2f      %-4s' % \
                (self.tag, atom_number, self.atomType, self.resName, residue_id,
                    x, y, z, self.weight, self.bFactor, chain_id)
        elif out_format == 'debug':
            output = '%-6s%5i %4s %4s %1s%4i    %8.3f%8.3f%8.3f' % \
                (segment_type, atom_number, self.atomType, self.resName, chain_id, residue_id,
                    x, y, z)
        elif out_format == 'xdebug':
            output = '%15s > %15s: %5i %4s%4s %1s%4i    %8.3f%8.3f%8.3f%6.2f%6.2f' % \
                (self.addr0, self.addr, atom_number, self.atomType, self.resName,
                    chain_id, residue_id, x, y, z)
        elif out_format == 'repr':
            output = '%15s :: %4s %4s    %8.3f %8.3f %8.3f' % \
                (self.addr, self.atomType, self.resName, x, y, z)
        elif out_format in ['crd', 'cor', 'card', 'short', 'shortcard']:
            output = '%5i%5i %-4s %-4s%10.5f%10.5f%10.5f %-4s %-4i%10.5f' % \
                (atom_number, self.residue_index, self.resName, self.atomType,
                    x, y, z, chain_id, residue_id, self.weight)
        elif out_format in ['xcrd', 'xcor', 'xcard', 'long', 'longcard']:
            output = '%10i%10i  %-4s      %-4s    %20.10f%20.10f%20.10f  %-5s    %-4i    %20.10f' % \
                (atom_number, self.residue_index, self.resName, self.atomType,
                    x, y, z, chain_id + segment_type, residue_id, self.weight)
        else:
            raise AtomError('to_str: unknown out_format %s' % out_format)
        return output.upper()
