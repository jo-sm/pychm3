from pychm3.const.bio import atomMass, backbone, charmm2pdbAtomNames, good, pro, nuc
from pychm3.lib.metaatom import AtomError
from pychm3.lib.baseatom import BaseAtom

class Atom(BaseAtom):
    """
    :Note:  This class is derived from :mod:`pychm.lib.baseatom`,
        please familiarize yourself with that documentation before
        proceeding with this article.


    The standard CHARMMing implementation of the all-atom resolution
    atom-like object.

    All **Core Data** defined by parent classes is also implicitly
    included.

    **Core Data:**
        | ``element``

    Both of these private methods are used exclusively by
    :mod:`pychm.scripts.parse`.

    **Private Methods:**
        | ``__charmm_compliant_residue_name``
        | ``_compliance_atomType``

    :TODO:
        | # Input and output *AMBER* style atomic data.
    """

    _autoInFormat = 'pdborg'
    """
    A string that defines the default input formatting for all class
    instances.
    """

    _sortSegType = {
        'pro': 1,
        'dna': 2,
        'rna': 3,
        'nuc': 4,
        'good': 5,
        'bad': 10
    }
    """
    This is a dictionary which defines the recognized `segTypes` (keys)
    for the `_sort` scoring method, and their weighting (values).
    """

    _tagMap = {
        'nuc': 'atom',
        'pro': 'atom',
        'good': 'atom',
        'bad': 'hetatm',
        'dna': 'atom',
        'rna': 'atom',
        'bln': 'atom'
    }
    """
    Map `segType` -> `tag`

    This is a dictionary which defines the recognized `segTypes` (keys)
    for the `tag` property.
    """

    def __init__(self, text=None, **kwargs):
        self.__element = 'auto'
        self.__segment_type = 'auto'

        self._properties.update({
          'element': 'x'
        })

        super().__init__(text, **kwargs)

    ####################
    # Parse Definition #
    ####################

    def parse(self, inFormat):
        """
        Parses ``self._text`` into the following instance variables:
            | ``atomNum``
            | ``atomType``
            | ``resName``
            | ``chainid``
            | ``resid``
            | ``cart``
            | ``weight``
            | ``bFactor``
            | ``element``
            | ``resIndex``

        Initial values for addressing are stored in:
            | ``chainid0``
            | ``segType0``
            | ``resid0``
            | ``atomNum0``

        Recognized *inFormat* values are:
            | ``"pdborg"``
            | ``"charmm"``
            | ``"shortcard"``
            | ``"longcard"``
            | ``"amber"``   **TODO**
        """
        if inFormat == 'pdborg':
            self.number = self._text[6:11]
            self.type = self._text[12:16]
            self.residue_name = self._text[16:20]
            self.chain_id = self._text[21]
            self.residue_id = self._text[22:26]
            self.cart = (self._text[30:38], self._text[38:46], self._text[46:54])
            self.weight = self._text[55:60]
            self.b_factor = self._text[61:66]
            self.residue_index = self.residue_id
        elif inFormat == 'charmm':
            self.number = self._text[6:11]
            self.residue_name = self._text[17:21]
            self.type = self._text[12:16]
            self.chain_id = self._text[72:76]
            self.residue_id = self._text[22:26]
            self.cart = (self._text[30:38], self._text[38:46], self._text[46:54])
            self.weight = self._text[55:60]
            self.b_factor = self._text[61:66]
            self.residue_index = self.residue_id
        elif inFormat in ['crd', 'cor', 'card', 'short', 'shortcard']:
            self.number = self._text[0:5]
            self.residue_index = self._text[5:10]
            # null [10:11]
            self.residue_name = self._text[11:15]
            # null [15:16]
            self.type = self._text[16:20]
            self.cart = (self._text[20:30], self._text[30:40], self._text[40:50])
            # null [50:51]
            self.chain_id = self._text[51:55]
            # null [55:56]
            self.residue_id = self._text[56:60]
            self.weight = self._text[60:70]
            self.b_factor = 1.
        elif inFormat in ['xcrd', 'xcor', 'xcard', 'long', 'longcard']:
            self.number = self._text[0:10]
            self.residue_index = self._text[10:20]
            # null [20:22]
            self.residue_name = self._text[22:30]
            # null [30:32]
            self.type = self._text[32:40]
            self.cart = (self._text[40:60], self._text[60:80], self._text[80:100])
            # null [100:102]
            self.chain_id = self._text[102:110]
            # null [110:112]
            self.residue_id = self._text[112:120]
            self.weight = self._text[120:140]
            self.b_factor = 1.
        elif inFormat == 'mol2':
            # FixMe: most MOL2 files break nicely into columns, and because the
            # atom number field is right justified, we read this column-wise rather
            # than position-wise. It would be better not to call cleanStrings and
            # do this by column.
            textarr = self._text.split()
            self.number = textarr[0]
            self.residue_index = textarr[6]
            self.residue_id = self.residue_index
            self.residue_name = textarr[7]
            self.type = textarr[1]
            self.element_type = textarr[5]
            self.cart = (textarr[2], textarr[3], textarr[4])
            self.chain_id = 'a'
            self.weight = 1.0
            self.b_factor = 1.
        else:
            raise AtomError('parse: unknown `inFormat`: %s' % inFormat)

        # Fix atom type padding
        self.__fix_atom_type()

        # Save initial properties
        self.__initial_props = {
            'number': self.number,
            'type': self.type,
            'chain_id': self.chain_id,
            'segment_type': self.segment_type,
            'residue_id': self.residue_id,
            'residue_name': self.residue_name
        }

    ##############
    # Properties #
    ##############

    # "Initial" properties
    @property
    def addr_init(self):
      return "{}.{}.{}.{}".format(
          self.__initial_props['chain_id'],
          self.__initial_props['segment_type'],
          self.__initial_props['residue_id'],
          self.__initial_props['number']
      )

    @property
    def number_init(self):
      return self.__initial_props['number']

    @property
    def type_init(self):
      return self.__initial_props['type']

    @property
    def chain_id_init(self):
      return self.__initial_props['chain_id']

    @property
    def segment_type_init(self):
      return self.__initial_props['segment_type']

    @property
    def residue_id_init(self):
      return self.__initial_props['residue_id']

    @property
    def residue_name_init(self):
      return self.__initial_props['residue_name']

    @property
    def addr(self):
      return "{}.{}.{}.{}".format(
          self.chain_id,
          self.segment_type,
          self.residue_id,
          self.number
      )

    @property
    def element(self):
        """
        A string which defines the cannonical 2 character element
        abbreviation.  If set to *'auto'*, it polls the ``atomType``
        property for this information.  Otherwise it is set manually.
        """
        if self.__element == 'auto':
            return self.type[:2].strip()
        else:
            return self.__element

    @element.setter
    def element(self, element):
        self.__element = str(element).strip().lower()

    @property
    def mass(self):
        """
        Polls the ``element`` property, and returns the mass in AMU of
        the :class:`Atom`.  Read only.
        """
        if hasattr(self, '__atom_mass'):
            return self.__atom_mass

        self.__atom_mass = atomMass[self.element]

        return self.__atom_mass

    @property
    def segment_type(self):
        """
        A string which defines the biochemical nature of the current
        segment.  Valid ``segTypes`` for the :class:`Atom` are:
        *"pro" "nuc" "rna" "dna" "good" "bad"*.  If set to *'auto'*,
        it polls the ``resName`` for this information.  Otherwise it
        is set manually.
        """
        if self.__segment_type != 'auto':
            return self.__segment_type
        else:
            if self.is_pro:
                return 'pro'
            elif self.is_nuc:
                return 'nuc'
            elif self.is_good:
                return 'good'
            else:
                return 'bad'

    @segment_type.setter
    def segment_type(self, type):
        self.__segment_type = str(type).strip().lower()

    @property
    def is_backbone(self):
        """
        Determines if the atom is part of the backbone structure using
        the ``atomType`` property.  Returns a :class:`bool`.
        """
        return self.type in backbone

    @property
    def is_good(self):
        """
        Determines if the atom is a "good" hetero atom using the
        ``resName`` property.  Returns a :class:`bool`.
        """
        return self.residue_name in good

    @property
    def is_pro(self):
        """
        Determines if the atom is part of a protein structure using the
        ``resName`` property.  Returns a :class:`bool`.
        """
        return self.residue_name[-3:] in pro

    @property
    def is_nuc(self):
        """
        Determines if the atom is part of a nucleic acid structure
        using the ``resName`` property.  Returns a :class:`bool`.
        """
        return self.residue_name[-3:] in nuc

    ##################
    # Public Methods #
    ##################

    def to_str(self, **kwargs):
        """
        Returns a string representation of the :class:`Atom`. Formatting
        defaults to `self.__class__._autoInFormat`.

        **kwargs:**
            | ``outformat``     ["pdborg","charmm","debug","xdebug","crd","xcrd"]
            | ``chain_id_init``   [False,True]
            | ``old_segtype``   [False,True]
            | ``residue_id_init``     [False,True]
            | ``atom_number_init``   [False,True]

        >>> thisAtom.Print(outformat="pdborg",residue_id_init=True)
        """
        out_format = kwargs.get('format', self.__class__._autoInFormat)
        chain_id_init = kwargs.get('chain_id_init', False)
        segment_type_init = kwargs.get('segment_type_init', False)
        residue_id_init = kwargs.get('residue_id_init', False)
        atom_number_init = kwargs.get('atom_number_init', False)

        # Localize variables
        x, y, z = self.cart

        if chain_id_init:
            chain_id = self.chain_id_init
        else:
            chain_id = self.chain_id

        if segment_type_init:
            segment_type = self.segment_type_init
        else:
            segment_type = self.segment_type

        if residue_id_init:
            residue_id = self.residue_id_init
        else:
            residue_id = self.residue_id

        if atom_number_init:
            number = self.number_init
        else:
            number = self.number

        # Unretrofit whitespace padding for long (hydrogen) atomType...
        if len(self.type) > 4:
            atom_type = self.type[-4:]
        else:
            atom_type = self.type

        # Set formatting
        if out_format == 'pdborg':
          format_string = "{tag:6s}{number:5d} {type:4s}{residue_name:>4s} {chain_id:1s}{residue_id:4d}    {x:8.3f}{y:8.3f}{z:8.3f}{weight:6.2f}{b_factor:6.2f}{element:>12s}"
        elif out_format == 'charmm':
          format_string = "{tag:6s}{number:5d} {type:4s} {residue_name:>4s} {residue_id:4d}    {x:8.3f}{y:8.3f}{z:8.3f}{weight:6.2f}{b_factor:6.2f}      {chain_id:4s}"
        elif out_format == 'debug':
          format_string = "{segment_type:6s}{number:5d} {type:>4s} {residue_name:>4s} {chain_id:1s}{residue_id:4d}    {x:8.3f}{y:8.3f}{z:8.3f}"
        elif out_format == 'xdebug':
          format_string = "{addr_init:>15s} > {addr:>15s}: {number:5d} {type:>4s}{residue_name:>4s} {chain_id:1s}{residue_id:4d}    {x:8.3f}{y:8.3f}{z:8.3f}{weight:6.2f}{b_factor:6.2f}"
        elif out_format == 'repr':
          format_string = "{addr:>15s} :: {type:>4s} {residue_name:>4s}    {x:8.3f}{y:8.3f}{z:8.3f}"
        elif out_format in ['crd', 'cor', 'card', 'short', 'shortcard']:
          format_string = "{number:5d}{residue_index:5d} {residue_name:4s} {type:4s}{x:10.5f}{y:10.5f}{z:10.5f} {chain_id:4s} {residue_id:>4d}{weight:10.5f}"
        elif out_format in ['xcrd', 'xcor', 'xcard', 'long', 'longcard']:
          format_string = "{number:10d}{residue_index:10d} {residue_name:8s} {type:8s}{x:20.10f}{y:20.10f}{z:20.10f} {chain_id_segment_type:8s} {residue_id:>8d}{weight:20.10f}"
        elif out_format == 'mol2':
          format_string = "{number:6d} {type:8s} {x:9.4f} {y:9.4f} {z:9.4f} {element_type:7s} {residue_id:2d} {residue_name:>4s}"
        elif out_format in ['xyz', 'xyz-qchem']:
          format_string = "{element:2s}  {x:14.5f} {y:14.5f} {z:14.5f}"
        else:
            raise AtomError('to_str: unknown format "%s"' % out_format)

        return format_string.format(**{
          'tag': self.tag,
          'number': number,
          'type': atom_type,
          'residue_index': self.residue_index,
          'residue_name': self.residue_name,
          'residue_id': residue_id,
          'chain_id': chain_id,
          'segment_type': segment_type,
          'chain_id_segment_type': (chain_id + segment_type),
          'x': x,
          'y': y,
          'z': z,
          'addr': self.addr,
          'addr_init': self.addr_init,
          'weight': self.weight,
          'b_factor': self.b_factor,
          'element': self.element,
          'element_type': self.element_type if hasattr(self, 'element_type') else None,
        }).upper()

    def charmm_compliance(self):
      self.__charmm_compliant_residue_name()
      self.__charmm_compliant_type()

    ###################
    # Private Methods #
    ###################

    def __charmm_compliant_residue_name(self):
        """
        Re-label ``resName`` string to be CHARMM compliant.
        """
        compliance_info = {
          'tip3': {
            'residue_name': [ 'hoh', 'tip3' ]
          },
          'hsd': {
            'residue_name': [ 'his' ]
          },
          'ade': {
            'residue_name': [ 'a', 'da' ]
          },
          'thy': {
            'residue_name': [ 't', 'dt' ]
          },
          'cyt': {
            'residue_name': [ 'c', 'dc' ]
          },
          'gua': {
            'residue_name': [ 'g', 'dg' ]
          },
          'ura': {
            'residue_name': [ 'u', 'du' ]
          },
          'heme': {
            'residue_name': [ 'hem' ]
          },
          'zn2': {
            'residue_name': [ 'zn' ],
            'type': [ 'zn  ' ]
          },
          'sod': {
            'residue_name': [ 'na' ],
            'type': [ 'na  ' ]
          },
          'ces': {
            'residue_name': [ 'cs' ],
            'type': [ 'cs  ' ]
          },
          'cla': {
            'residue_name': [ 'cl' ],
            'type': [ 'cl  ' ]
          },
          'cal': {
            'residue_name': [ 'ca' ],
            'type': [ 'ca  ' ]
          },
          'pot': {
            'residue_name': [ 'k' ],
            'type': [ ' k  ' ]
          }
        }

        self.__update_compliance('residue_name', compliance_info)

    def __charmm_compliant_type(self):
      compliance_info = {
        'oh2': {
          'residue_name': [ 'hoh', 'tip3' ]
        },
        ' cd ': {
          'residue_name': [ 'ile' ],
          'type': [ ' cd1' ]
        },
        'sod ': {
          'residue_name': [ 'na', 'sod' ],
          'type': [ 'na  ' ]
        },
        'ces ': {
          'residue_name': [ 'cs', 'ces' ],
          'type': [ 'cs  ' ]
        },
        'cla ': {
          'residue_name': [ 'cl', 'cla' ],
          'type': [ 'cl  ' ]
        },
        'cal ': {
          'residue_name': [ 'ca', 'cal' ],
          'type': [ 'cal ' ]
        },
        'pot ': {
          'residue_name': [ 'k', 'pot' ],
          'type': [ ' k  ' ]
        }
      }

      self.__update_compliance('type', compliance_info)

    # Private methods

    def __fix_atom_type(self):
        """
        Fixes the white space padding on ``type`` strings to
        conform to the 4 character standard.

        Where the first 2 characters represent the element, and the
        last 2 characters the chemical environment.  It accounts for
        the common exception to this rule where 1 character elements
        may use 3 characters for their chemical environment.
        """

        stripped_type = self.type.strip()

        if self.segment_type == 'good':
          if charmm2pdbAtomNames.get(stripped_type):
            self.type = charmm2pdbAtomNames[stripped_type]
          else:
            if len(stripped_type) == 4:
              self.type = " {}".format(stripped_type)

            elif len(stripped_type) in [ 2, 3 ]:
              self.type = '{}  '.format(stripped_type)[:4]

            else:
              self.type = ' {}  '.format(stripped_type)[:4]

        elif self.segment_type == 'pro':
          if len(stripped_type) == 4:
            self.type = ' {}'.format(stripped_type)
          else:
            self.type = ' {}  '.format(stripped_type)[:4]

    def __update_compliance(self, attr, info):
      checks_pass = 0

      for name, checks in list(info.items()):
        num_checks = len(list(checks.keys()))
        checks_pass = 0

        for check_name, check_values in list(checks.items()):
          if hasattr(self, check_name):
            if getattr(self, check_name) in check_values:
              checks_pass += 1

        if checks_pass == num_checks:
          setattr(self, attr, name)
          break
