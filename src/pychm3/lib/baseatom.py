from pychm3.const import alphanum
from pychm3.lib.metaatom import AtomError, MetaAtom

class BaseAtom(MetaAtom):
    """
    This is the base class for all "CHARMM-like" atoms.  It includes
    property definitions and methods useful for classical mechanics
    based molecular models.  However, like :mod:`pychm.lib.metaatom`,
    :mod:`baseatom` is still abstract, and thus not useful without
    deriving and defining the methods marked **STUB** herein.

    All **Core Data** defined by parent classes is also implicitly
    included.

    **Core Data:**
        | ``number``
        | ``type``
        | ``bFactor``
        | ``chain_id``
        | ``residue_id``
        | ``residue_index``
        | ``residue_name``
        | ``segment_type``
        | ``weight``

    As some **STUBS** are defined in this class, and others are added,
    the following list explicitly states all **STUBS** that must be
    defined by child classes.

    **STUBS:**
        | ``_autoInFormat``
        | ``_sortSegType``
        | ``_tagMap``
        | ``parse``
        | ``Print``

    This private method has been documented as a **Special Method** because
    it defines the scoring system used by the rich comparison operators.

    The weighting for this scoring functions is:
        ``chainid`` > ``segType`` > ``resid`` > ``atomNum``

    Where ``chainid`` is sorted from `a` to `9`, ``segType`` is sorted
    according to the :class:`dict` defined by ``self.__class__._sortSegType``
    and ``resid`` and ``atomNum`` are straightforward.

    **Special Methods:**
        | ``_sort``
    """

    _sortSegType = None
    """
    This is a dictionary which defines the recognized ``segTypes`` (keys)
    for the ``_sort`` scoring method, and their weighting (values).

    **STUB**
    """

    _tagMap = None
    """
    Map ``segType`` -> ``tag``

    This is a dictionary which defines the recognized `segTypes` (keys)
    for the `tag` property.

    **STUB**
    """

    def __init__(self, text=None, **kwargs):
      self._properties.update({
        'number': 0,
        'type': 'unkt',
        'b_factor': 0,
        'chain_id': '?',
        'residue_id': 0,
        'residue_index': 0,
        'residue_name': 'unkr',
        'segment_type': 'bad',
        'weight': 0
      })
      super().__init__(text, **kwargs)

##############
# Properties #
##############

    @property
    def addr(self):
        """
        The ``addr`` property provides a human readable unique string
        representation for each `BaseAtom` instance.

        The default is: "``chain_id.segment_type.residue_id.number``"
        """
        return "{}.{}.{}.{}".format(
            self.chain_id,
            self.segment_type,
            self.residue_id,
            self.number
        )

    @property
    def number_init(self):
        """
        The value of the ``number`` property at instantization, read only.
        """
        return self.__initial_props['number']

    @property
    def type(self):
        """
        The four character string which defines the atomic element,
        and neighboring chemical environment.  Typically the first
        two characters define the element, and the final two define
        the chemical environment.  However many sources do not adhere
        to this rule.

        **Note:** ``atomType`` should always be 4 characters long,
        and whitespace should never be stripped.
        Remember kids, ``' CA ' != 'CA  '``.

        **Note:** The one exception to the above rule is for elements
        which have a single character abbreviation, and might have 3
        characters denoting their type, for example ``HD22``.  Other
        single character elements should also work.
        """
        return self.__type

    @type.setter
    def type(self, atom_type):
        atom_type = str(atom_type).lower()

        if len(atom_type) > 5:
            if self._autoFix:
                atom_type = atom_type[:5]
            else:
                raise AtomError("Atom type is longer than 5 characters: {}".format(atom_type))

        self.__type = atom_type

    @property
    def type_init(self):
        """
        The value of the ``type`` property at instantization, read only.
        """
        return self.__initial_props['type']

    @property
    def b_factor(self):
        """
        A float in the range [0,100].  It is a proxy for the quality of the
        structural model.  Lower values are better.
        """
        return self.__b_factor

    @b_factor.setter
    def b_factor(self, b_factor):
        try:
            b_factor = float(b_factor)
        except ValueError:
            if self._autoFix:
                b_factor = 100.0
            else:
                raise AtomError("Atom b factor value is invalid: {}".format(b_factor))

        if b_factor > 100.:
            if self._autoFix:
                b_factor = 100.
            else:
                raise AtomError('Atom b factor value greater than 100.0: {}'.format(b_factor))
        elif b_factor < 0.:
            if self._autoFix:
                b_factor = 0.
            else:
                raise AtomError('Atom b factor value less than 0.0: {}'.format(b_factor))

        self.__b_factor = b_factor

    @property
    def chain_id(self):
        """
        A single character string indicating the domain of the structure
        where the atom is located.  It is in the range of [`'a'`,`9`].
        """
        return self.__chain_id

    @chain_id.setter
    def chain_id(self, chain_id):
        chain_id = str(chain_id).strip().lower()

        if len(chain_id) > 1:
            if self._autoFix:
                chain_id = chain_id[0]
            else:
                raise AtomError('Atom chain ID is longer than 1 character: {}'.format(chain_id))

        self.__chain_id = chain_id

    @property
    def chain_id_init(self):
        """
        The value of the `chainid` property at instantization, read only.
        """
        return self.__initial_props['chain_id']

    @property
    def residue_id(self):
        """
        An integer in the range of [-1000,10000].  It denotes the residue
        by number, and to facilitate CHARMM jobs, it is typically reindexed
        to start at one for each segment.  To retrieve the cannonical
        resid refer to ``resid0``.
        """
        return self.__residue_id

    @residue_id.setter
    def residue_id(self, residue_id):
        residue_id = int(residue_id)

        if residue_id < -1000:
            if self._autoFix:
                residue_id = -1000
            else:
                raise AtomError('Atom residue ID is less than -1000: {}'.format(residue_id))
        elif residue_id > 10000:
            if self._autoFix:
                residue_id = 10000
            else:
                raise AtomError('Atom residue ID is greater than 10000: {}'.format(residue_id))

        self.__residue_id = residue_id

    @property
    def residue_id_init(self):
        """
        The value of the ``residue_id`` property at instantization, read only.
        """
        return self.__initial_props['residue_id']

    @property
    def residue_index(self):
        """
        An integer in the range of [-1000,10000].  It denotes the residue
        by number and is indexed from 1.  Unlike the ``resid`` it indexes
        the residues present in the entire molecule, not just in the
        current segment.
        """
        return self.__residue_index

    @residue_index.setter
    def residue_index(self, index):
        index = int(index)
        if index < -1000:
            if self._autoFix:
                index = -1000
            else:
                raise AtomError('Atom residue index less than -1000: {}'.format(index))
        elif index > 10000:
            if self._autoFix:
                index = 10000
            else:
                raise AtomError('Atom residue index greater than 10000: {}'.format(index))
        self.__residue_index = index

    @property
    def residue_name(self):
        """
        A 4 character string denoting the name of the residue.  As per
        the PDB specification the first character is reserved for multi-
        model sections of PDB files, and the last 3 characters are used
        for the actual residue name itself.  This convention is loosely
        adhered to.
        """
        return self.__residue_name

    @residue_name.setter
    def residue_name(self, name):
        name = str(name).strip().lower()

        if len(name) > 4:
            if self._autoFix:
                name = name[:4]
            else:
               raise AtomError('Atom residue name is longer than 4 characters: {}'.format(name))

        self.__residue_name = name

    @property
    def residue_name_init(self):
        """
        The value of the `residue_name` property at instantization, read only.
        """
        return self.__initial_props['residue_name']

    @property
    def segment_id(self):
        """
        A unique string generated from the segment's parent chain_id and
        the segment's type.  Read only.
        """
        return "{}-{}".format(self.chain_id, self.segment_type)

    @property
    def segment_type(self):
        """
        A string which defines the biochemical nature of the current
        segment.  Valid ``segTypes`` should be registered as keys in
        ``self.__class__._sortSegType``.
        """
        return self.__segment_type

    @segment_type.setter
    def segment_type(self, segment_type):
        self.__segment_type = str(segment_type).strip().lower()

    @property
    def segment_type_init(self):
        """
        The value of the `segment_type` property at instantization, read only.
        """
        return self.__initial_props['segment_type']

    @property
    def tag(self):
        """
        The first field which appears in a PDB line, usually 'ATOM' or
        'HETATM'.  A mapping between ``segType`` and ``tag`` should be
        defined by a dictionary ``self.__class__._tagMap``.  Read only.
        """
        return self.__class__._tagMap[self.segment_type]

    @property
    def weight(self):
        """
        In multi-model sections of .pdb files the ``weight`` property
        defines the experimental confidence of the atomic coordinates.
        When stripping out multi-model sections of a .pdb, the ``weight``
        property is used to determine the most likely atomic coordinates
        and removes the others.
        """
        return self.__weight

    @weight.setter
    def weight(self, weight):
        try:
            weight = float(weight)
        except ValueError:
            if self._autoFix:
                weight = 0.
            else:
                raise AtomError('Atom weight must be float')

        if weight > 1.:
            if self._autoFix:
                weight = 1.
            else:
                raise AtomError('Atom weight is greater than 1.0: {}'.format(weight))
        elif weight < 0.:
            if self._autoFix:
                weight = 0.
            else:
                raise AtomError('Atom weight is less than 0.0: {}'.format(weight))

        self.__weight = weight

    @property
    def number(self):
        """
        An atomic index which spans all atoms in a single model or
        :class:`Mol` object.
        """
        if not self.__number:
            self.__number = 0

        return self.__number

    @number.setter
    def number(self, number):
        try:
            number = int(number)
        except ValueError:
            raise AtomError('Atom number must be an integer')

        if number < 0:
            if self._autoFix:
                number = 0
            else:
                raise AtomError('Atom number given is less than 0: {}'.format(number))
        elif number > 10000:
            if self._autoFix:
                number = 10000
            else:
                raise AtomError('Atom number given is greater than 10,000: {}'.format(number))

        self.__number = number


###################
# Private Methods #
###################

    def _sort(self):
        """
        Scoring method that determines sorting order, for rich
        comparisons and containerClass.sort() methods.

        The weighting for this scoring functions is as follows:
            ``chainid`` > ``segType`` > ``resid`` > ``atomNum``
        """
        sortChainid = dict(((char,i) for i,char in enumerate(alphanum)))
        return sortChainid[self.chain_id] * 1e10 + \
                self.__class__._sortSegType[self.segment_type] * 1e9 + \
                self.residue_id * 1e5 + self.number
