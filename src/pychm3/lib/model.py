from pychm3.lib.basestruct import BaseStruct
from pychm3.lib.chain import Chain
from pychm3.exceptions import MolError
from pychm3.utils.struct import reindex

class Model(BaseStruct):
    """
        **Private Methods:**
            | ``_compliance_charmmTerminalOxygen``
            | ``_compliance_charmmNames``
            | ``_diff_nuc``
            | ``_fix_multi_models``

        :TODO:
            | ``write``
    """
    def __init__(self, iterable=None, **kwargs):
        super().__init__(iterable, **kwargs)
        self.warnings = []

    ##################
    #   Properties   #
    ##################

    @property
    def chains(self):
      chain_ids = list(set([ atom.chain_id for atom in self ]))
      chain_ids.sort()

      iterators = [ [ atom for atom in self if atom.chain_id == chain_id ] for chain_id in chain_ids ]
      chains = [ Chain(iterable=iterator, code=self.code, autoFix=False) for iterator in iterators ]

      return chains

    @property
    def segments(self):
      return [segment for chain in self.chains for segment in chain.segments]

    @property
    def residues(self):
      return [residue for segment in self.segments for residue in segment.residues]

    @property
    def atoms(self):
      return [atom for atom in self]

    ##################
    # Public Methods #
    ##################

    def get_noCharge(self):
        """
        Returns a :class:`BaseStruct` containing :class:`Atom` objects
        for which the ``charge`` attribute is undefined.
        """
        return BaseStruct(( atom for atom in self if not hasattr(atom, 'charge') ))

    def make_charmm_compliant(self):
        """
        Performs all of the functions of the CHARMMing parser.

        * Multi-model Cleanup
        * Differentiate Nucleic Acids
        * Sorting
        * Renaming as per CHARMM conventions and compatability
        """
        self.__fix_multi_models()
        self.__fix_dupl_residues()
        self.__differentiate_dna_rna()
        self.sort()
        self.__charmm_compliant_terminal_oxygens()
        self.__charmm_compliant_names()

    def populate_charges(self, RTFFile):
        """
        Takes a :class:`RTFFile` object and uses it to define the :attr:`Atom.charge`
        attribute.  **It is strongly encouraged to call the :meth:`Mol.parse` method
        before calling this method.**
        """
        chargeDict = RTFFile.chargeResiDict
        for atom in self:
            try:
                atom.charge0 = chargeDict[atom.resName][atom.atomType.strip()]
                atom.charge = atom.charge0
            except KeyError:
                pass
        # now populate atoms which dont have a charge, but are not "bad"
        iterator = ( atom for atom in self if not hasattr(atom, 'charge0') and atom.segType != 'bad' )
        for atom in iterator:
            try:
                atom.charge0 = chargeDict[atom.resName0][atom.atomType0.strip()]
                atom.charge = atom.charge0
            except KeyError:
                pass

    def write_pychm(self):
        """
        Macro for writing CHARMMing style output.
        """
        segDict = {
          'nuc': 'nuc',
          'pro': 'pro',
          'good': 'goodhet',
          'bad': 'het',
          'dna': 'dna',
          'rna': 'rna'
        }

        stdoutList = []
        # Write files
        for seg in self.iter_seg():
            stdoutList.append('%s-%s' % (seg.chainid, segDict[seg.segType]))
            name = 'new_%s-%s-%s.pdb' % (self.pdbCode, seg.chainid, segDict[seg.segType])
            seg.write(filename=name, outformat='charmm', ter=True, end=False)
        # To STDOUT
        print('natom=%d' % len(self))
        print('nwarn=%d' % len(self.warnings))
        if self.warnings:
            print('warnings=%r' % self.warnings)
        print('seg=%r' % stdoutList)

    ###################
    # Private Methods #
    ###################

    def __charmm_compliant_terminal_oxygens(self):
        """
        Fix the atom types of terminal oxygen atoms to be CHARMM
        compliant.
        """
        segments = [ segment for segment in self.segments if segment.type == 'pro' ]
        for segment in segments:
          last_residue = segment.residues[-1]

          for atom in last_residue.atoms:
              if atom.type == ' o  ':
                  atom.type = ' ot1'
              elif atom.type == ' oxt':
                  atom.type = ' ot2'

    def __charmm_compliant_names(self):
        """
        On a per atom basis, fix residue names and atom types to be
        CHARMM compliant.
        """
        for atom in self:
            atom.charmm_compliance()

    def __differentiate_dna_rna(self):
        """
        Loop over all the segments in the Mol, and differentiate them
        between dna/rna, then set their seg type appropriately. Throw a
        warning if the determination cannot be made. The list of
        warnings is returned, segType is set inline.
        """
        badSegAddr = []
        warnings = []
        # Use Thymine/Uracil to determine dna/rna.
        thyCheck = set(['t', 'thy', 'dt'])
        uraCheck = set(['u', 'ura', 'du'])
        riboCheck = set(['a', 'c', 'g', 'u', 'i'])
        deoxyCheck = set(['da', 'dc', 'dg', 'dt', 'di'])

        segments = [ segment for segment in self.segments if segment.type == 'nuc' ]

        for segment in segments:
            thymine = False
            uracil = False

            for atom in segment.atoms:
                if atom.residue_name in thyCheck:
                    thymine = True
                if atom.residue_name in uraCheck:
                    uracil = True

                if atom.residue_name in riboCheck:
                    ribo = True
                if atom.residue_name in deoxyCheck:
                    deoxy = True

            if thymine and uracil:
              # Throw a warning when both Uracil and Thymine are found in the same segment
                msg = '_diff_nuc: URA & THY in same segment, seg.addr = "{}"'.format(segment.addr)
                badSegAddr.append(segment.addr)
                warnings.append(msg)
            elif ribo and deoxy:
              # Throw a warning when ribo- and deoxy- are found in the same segment
                msg = '_diff_nuc: ribo- & deoxy- in same segment, seg.addr = "{}"'.format(segment.addr)
                badSegAddr.append(segment.addr)
                warnings.append(msg)
            elif thymine:
                segment.type = 'dna'
            elif uracil:
                segment.type = 'rna'
            elif ribo:
                segment.type = 'rna'
            elif deoxy:
                segment.type = 'dna'

        # Do a final check
        segments = [ segment for segment in self.segments if segment.type == 'nuc' ]

        for segment in segments:
          if segment.addr not in badSegAddr:
              msg = '_diff_nuc: undetermined nucleotide, seg.addr = "{}"'.format(segment.addr)
              badSegAddr.append(segment.addr)
              warnings.append(msg)
          self.warnings = warnings

    def __fix_multi_models(self):
        """
        Searches for multi-model sections, and "fixes" them. Renames
        residues, and deletes atoms.

        For example:

        ``15  CE AMET A   1      16.764  26.891  42.696  0.73 60.01           C``
        ``16  CE BMET A   1      18.983  29.323  38.169  0.27 60.30           C``

        Becomes:

        ``15  CE  MET A   1      16.764  26.891  42.696  0.73 60.01           C``

        **Note:**
            | This implementation currently iterates over `segments` and
            | not `residues`.  The upshot of this is that there is a nasty
            | corner case where you might have a break in a residue, only
            | the resnames are such that an atom is erroneously deleted.
            | An example to illuminate...

        For example:

        ``15  CE AMET A   1      16.764  26.891  42.696  0.73 60.01           C``
        ``16  CE BMET A   2      18.983  29.323  38.169  0.27 60.30           C``

        Should become:

        ``15  CE  MET A   1      16.764  26.891  42.696  0.73 60.01           C``
        ``16  CE  MET A   2      18.983  29.323  38.169  0.27 60.30           C``

        Instead becomes:

        ``15  CE  MET A   1      16.764  26.891  42.696  0.73 60.01           C``
        """
        delete_me = []
        segments = [ segment for segment in self.segments if segment.type == 'pro' ]

        for segment in segments:
            tmp = []

            for i, atom in enumerate(segment.atoms):
                try:
                    # check if the resname is 4 chars long
                    if len(atom.residue_name) < 4:
                        continue

                    # Comparison atom
                    comp = segment[i + 1]
                    if len(comp.residue_name) < 4:
                        raise AssertionError
                    # check first char of comp atom
                    # if comp is bigger, buffer this atom
                    if atom.residue_name[0] < comp.residue_name[0]:
                        tmp.append(atom)
                    else:
                        raise AssertionError
                except (AssertionError, IndexError):
                    tmp.append(atom)
                    maxWeight = max(( atom1.weight for atom1 in tmp ))
                    # Write Notes
                    for atom1 in tmp:
                        if atom1.weight == maxWeight:
                            atom1.note = 'rename'
                            break
                        else:
                            atom1.note = 'delete'
                    # Process Notes
                    for atom1 in tmp:
                        try:
                            if atom1.note == 'rename':
                                atom1.residue_name = atom1.residue_name[1:]
                            elif atom1.note == 'delete':
                                delete_me.append(atom1)
                            else:
                                raise MolError
                            del atom1.note
                        except AttributeError:
                            delete_me.append(atom1)
                    tmp = []
        self.del_atoms(delete_me)

    def __fix_dupl_residues(self):
       """
       Fixes the case where we have a residue defined
       twice because the experimentalists could not
       differentiate between two closely-releated residues
       e.g. ASN and ILE. This happens in some PDBs such
       as 1DBA.
       """
       segments = [ segment for segment in self.segments if segment.type == 'pro' ]

       for segment in segments:
           delete_me = []
           prev_resid = -1
           prev_resnm = 'ZZZQ'

           for atom in segment.atoms:
               if prev_resid == atom.residue_id and prev_resnm != atom.residue_name:
                   print(('Delete: %s' % atom))
                   delete_me.append(atom)
               else:
                   prev_resid = atom.residue_id
                   prev_resnm = atom.residue_name

           self.del_atoms(delete_me)
