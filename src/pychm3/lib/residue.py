from pychm3.lib.basestruct import BaseStruct

from numpy import array as numpy_array

class Residue(BaseStruct):
    """
    Properties
        `addr`
        `chainid`
        `heavyCom`
        `resid`
        `resIndex`
        `resName`
        `segid`
        `segType`
    """
    def __init__(self, iterable=None, **kwargs):
        super().__init__(iterable, **kwargs)

##################
# Public methods #
##################

    def make_compliant(self):
        """Change resName to be CHARMM compliant."""
        # This function handles charmm renaming for everything _except_
        # terminal residue renaming. This is because this class doesn't
        # know if its members are in the terminal residue. This is ugly,
        # we may want to change it.
        if self.name in ['hoh', 'tip3']: # Water renaming
            self.name = 'tip3'
        elif self.name == 'his': # HIS -> HSE
            self.name = 'hsd'
        elif self.name in ['a', 'da']:
            self.name = 'ade'
        elif self.name in ['t', 'dt']:
            self.name = 'thy'
        elif self.name in ['c', 'dc']:
            self.name = 'cyt'
        elif self.name in ['g', 'dg']:
            self.name = 'gua'
        elif self.name in ['u', 'du']:
            self.name = 'ura'
        elif (self.name == 'zn'): # Ion renaming scheme
            self.name = 'zn2'
        elif (self.name == 'na'):
            self.name = 'sod'
        elif (self.name == 'cs'):
            self.name = 'ces'
        elif (self.name == 'cl'):
            self.name = 'cla'
        elif (self.name == 'ca'):
            self.name = 'cal'
        elif (self.name == 'k'):
            self.name = 'pot'
        return
##############
# Properties #
##############

    @property
    def is_good(self):
        """
        Determines if the atom is a "good" hetero atom using the
        ``resName`` property.  Returns a :class:`bool`.
        """
        return self.atoms[0].is_good

    @property
    def is_pro(self):
        """
        Determines if the atom is part of a protein structure using the
        ``resName`` property.  Returns a :class:`bool`.
        """
        return self.atoms[0].is_pro

    @property
    def is_nuc(self):
        """
        Determines if the atom is part of a nucleic acid structure
        using the ``resName`` property.  Returns a :class:`bool`.
        """
        return self.atoms[0].is_nuc

    @property
    def addr(self):
        """
        The `addr` property provides a human readable unique string
        representation for each `Res` instance.
        """
        return "{}.{}.{}".format(self.chain_id, self.segment_type, self.id)

    @property
    def heavy_com(self):
        """
        The center of mass of residue, computed using only "heavy"
        (non-hydrogen) atoms.

        Care should be taken with this method, as it filters with
        `BaseAtom.element` which won't necesarily be defined for all
        atoms.
        """
        result = numpy_array([atom.mass * atom.cart for atom in self.atoms if atom.element == 'h'])
        result = result.sum(axis=0)
        mass = sum(atom.mass for atom in self.atoms if atom.element == 'h')

        return result / mass

    @property
    def residue_id_init(self):
        return self.atoms[0].residue_id_init

    @property
    def atoms(self):
      return [atom for atom in self]

    @property
    def id(self):
      return self.atoms[0].residue_id

    @id.setter
    def id(self, id):
        for atom in self.atoms:
            atom.residue_id = id

    @property
    def index(self):
      return self.atoms[0].residue_index

    @index.setter
    def index(self, index):
        for atom in self.atoms:
            atom.residue_index = index

    @property
    def name(self):
      return self.atoms[0].residue_name

    @name.setter
    def name(self, name):
        for atom in self.atoms:
            atom.residue_name = name

    @property
    def segment_id(self):
      return self.atoms[0].segment_id

    @property
    def segment_type(self):
      return self.atoms[0].segment_type

    @property
    def chain_id(self):
      return self.atoms[0].chain_id

###################
# private methods #
###################

    def _dogmans_rename(self):
        elements = set(atom.element for atom in self.atoms)
        for this_element in elements:
            tmp = [atom for atom in self if atom.element == this_element]
            if len(tmp) == 1:
                tmp[0].type = this_element
            else:
                for i, atom in enumerate(tmp):
                    atom.type = "%s%d" % (this_element, i+1)
