from pychm3.lib.toppar import BasePRM
from pychm3.utils.numeric import loose_float

import warnings

class Mass(BasePRM):
    """Object representing particle mass data.

    The first argument is a string representing the atomtype. The second
    argument (this might be changed to the last arg, and be made optional) is
    the unique integer ID used by CHARMM. The third argument is a float
    representing the mass in AMU. The fourth argument is an optional string
    representing the elemental symbol.
    """
    __slots__ = ['atom', 'id', 'mass', 'element', 'comment']

    def __init__(self, id=None, atom=None, mass=None, element=None, comment=None):
        self.id = int(id)
        self.atom = atom
        self.mass = loose_float(mass)
        self.element = element
        self.comment = comment

    @property
    def _sortkey(self):
        return (self.atom,)

    def _validate(self):
        for attrname in self.__slots__[:3]:
            if getattr(self, attrname) is None:
                warnings.warn("%r has uninitialized attr: %s" % (self, attrname))
