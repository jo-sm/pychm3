from pychm3.lib.toppar import Residue

class Patch(Residue):
    """Object representing patch residue information. Mostly a black box.

    The first two arguments are the residue name and its total charge. The
    remaining information is stored as a black box in the third argument. This
    will likely change in the near future and a fully featured residue object
    will be implemented.
    """
    @property
    def _sortkey(self):
        return "b%r" % self.name
