from pychm3.lib.toppar import BasePRM
from pychm3.utils.numeric import loose_float

import warnings

class PRMAngle(BasePRM):
    """Object representing angle bending parameter constants.

    The first three arguments are strings representing atomtypes. During
    initialization these strings are normalized, such that 'atom0' is always
    the "lesser" string, allowing AnglePRM objects to be hashed correctly.
    Changing the 'atom' attributes after initialization is *not* recommended
    because uniqueness is not guaranteed. For example, Angle(ABC) == Angle(CBA)
    if set at initialization, but not equal if set afterwards.  Changing the
    spring constant, 'k' and the equilibrium bond length 'eq' is fine at any
    time. The 1-3 interaction parameters may be optionally set.
    """
    __slots__ = ['atom0', 'atom1', 'atom2', 'k', 'eq', 'k13', 'eq13', 'comment']

    def __init__(self, atom0=None, atom1=None, atom2=None, k=None, eq=None, k13=None, eq13=None, comment=None):
        self.atom0, self.atom2 = sorted((atom0, atom2))
        self.atom1 = atom1
        self.k = loose_float(k)
        self.eq = loose_float(eq)
        self.k13 = loose_float(k13)
        self.eq13 = loose_float(eq13)
        self.comment = comment

    @property
    def _sortkey(self):
        return (self.atom0, self.atom1, self.atom2)

    def _validate(self):
        for attrname in self.__slots__[:5]:
            if getattr(self, attrname) is None:
                warnings.warn("%r has uninitialized attr: %s" % (self, attrname))
        if self.k13 is None and self.eq13 is not None:
            warnings.warn("%r has uninitialized attr: k13" % self)
        if self.eq13 is None and self.k13 is not None:
            warnings.warn("%r has uninitialized attr: eq13" % self)
