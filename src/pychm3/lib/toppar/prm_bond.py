from pychm3.lib.toppar import BasePRM
from pychm3.utils.numeric import loose_float

import warnings

class PRMBond(BasePRM):
    """Object representing bonding parameter constants.

    The first two arguments are strings representing atomtypes. During
    initialization these strings are normalized, such that 'atom0' is always
    the "lesser" string, allowing BondPRM objects to be hashed correctly.
    Changing the 'atom' attributes after initialization is *not* recommended
    because uniqueness is not guaranteed. For example, Bond(AB) == Bond(BA) if
    set at initialization, but not equal if set afterwards. Changing the spring
    constant, 'k' and the equilibrium bond length 'eq' is fine at any time.
    """
    __slots__ = ['atom0', 'atom1', 'k', 'eq', 'comment']

    def __init__(self, atom0=None, atom1=None, k=None, eq=None, comment=None):
        self.atom0, self.atom1 = sorted((atom0, atom1))
        self.k = loose_float(k)
        self.eq = loose_float(eq)
        self.comment = comment

    @property
    def _sortkey(self):
        return (self.atom0, self.atom1)

    def _validate(self):
        for attrname in self.__slots__[:4]:
            if getattr(self, attrname) is None:
                warnings.warn("%r has uninitialized attr: %s" % (self, attrname))
