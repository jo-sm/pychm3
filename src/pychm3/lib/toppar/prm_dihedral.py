from pychm3.lib.toppar import BasePRM
from pychm3.utils.numeric import loose_float, loose_int

import warnings

class PRMDihedral(BasePRM):
    """Object representing dihedral angle parameter constants.

    The first four arguments are strings representing atomtypes. During
    initialization these strings are normalized, such that 'atom0' is always
    the "lesser" string, allowing DihedralPRM objects to be hashed correctly.
    Changing the 'atom' attributes after initialization is *not* recommended
    because uniqueness is not guaranteed. For example, Dihedral(ABCD) ==
    Dihedral(DCBA) if set at initialization, but not equal if set afterwards.
    Furthermore, the multiplicity of the dihedral, 'mult' is also used when
    determining parameter uniqueness. Changing the spring constant, 'k' and the
    equilibrium bond length 'eq' is fine at any time.
    """
    __slots__ = ['atom0', 'atom1', 'atom2', 'atom3' 'k', 'mult', 'eq', 'comment']

    def __init__(self, atom0=None, atom1=None, atom2=None, atom3=None, k=None, mult=None, eq=None, comment=None):
        self.atom0, self.atom3 = sorted((atom0, atom3))
        if self.atom0 == atom0:
            self.atom1, self.atom2 = (atom1, atom2)
        else:
            self.atom1, self.atom2 = (atom2, atom1)
        self.k = loose_float(k)
        self.mult = loose_int(mult)
        self.eq = loose_float(eq)
        self.comment = comment

    @property
    def _sortkey(self):
        return (self.atom0, self.atom1, self.atom2, self.atom3, self.mult)

    def _validate(self):
        for attrname in self.__slots__[:7]:
            if getattr(self, attrname) is None:
                warnings.warn("%r has uninitialized attr: %s" % (self, attrname))
