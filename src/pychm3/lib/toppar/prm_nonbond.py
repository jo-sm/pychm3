from pychm3.lib.toppar import BasePRM
from pychm3.utils.numeric import loose_float

import warnings

class PRMNonbond(BasePRM):
    """Object representing unary nonbonding VDW parameter constants.

    The first argument is a string representing the atomtype. The next three
    arguments, 'ig', 'k' and 'eq' are ignored, the LJ 6-12 well depth and the
    equilibrium interaction distance upon two, respectively.  The last three
    arguments are optional, and only used for calculating 1-4 nonbonded
    interactions.
    """
    __slots__ = ['atom', 'ig', 'k', 'eq', 'ig14', 'k14', 'eq14', 'comment']

    def __init__(self, atom=None, ig=None, k=None, eq=None, ig14=None, k14=None, eq14=None, comment=None):
        self.atom = atom
        self.ig = loose_float(ig)     # ignored
        self.k = loose_float(k)       # epsilon
        self.eq = loose_float(eq)     # r_min/2
        self.ig14 = loose_float(ig14)
        self.k14 = loose_float(k14)
        self.eq14 = loose_float(eq14)
        self.comment = comment

    @property
    def _sortkey(self):
        return (self.atom,)

    def _validate(self):
        for attrname in self.__slots__[:4]:
            if getattr(self, attrname) is None:
                warnings.warn("%r has uninitialized attr: %s" % (self, attrname))
        if self.ig14 is None and (self.k14 is not None or self.eq14 is not None):
            warnings.warn("%r has uninitialized attr: ig14" % self)
        if self.k14 is None and (self.ig14 is not None or self.eq14 is not None):
            warnings.warn("%r has uninitialized attr: k14" % self)
        if self.eq14 is None and (self.ig14 is not None or self.k14 is not None):
            warnings.warn("%r has uninitialized attr: eq14" % self)
