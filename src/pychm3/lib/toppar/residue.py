from pychm3.lib.toppar import BasePRM
from pychm3.utils.numeric import loose_float

class Residue(BasePRM):
    """Object representing residue information. Mostly a black box.

    The first two arguments are the residue name and its total charge. The remaining
    information is stored as a black box in the third argument. This will likely change
    in the near future and a fully featured residue object will be implemented.
    """
    def __init__(self, name=None, charge=None, body=None, comment=None):
        self.name = name
        self.charge = loose_float(charge)
        self.body = body
        self.comment = comment

    @property
    def _sortkey(self):
        return "a%r" % self.name

    def _validate(self):
        pass

    def __repr__(self):
        return '%s(%s)' % (self.__class__.__name__, self.name)
