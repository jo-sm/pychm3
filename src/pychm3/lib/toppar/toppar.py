from pychm3.utils.toppar import merge_mass, merge_cmap, merge_section, merge_command, unique as _unique

class Toppar(object):
    """The class containing chemical data related to molecular topology and
    bonding parameters.
    """
    data_sections = [
        'bond',
        'angle',
        'dihedral',
        'improper',
        'cmap',
        'nonbond',
        'nbfix',
        'hbond',
        'mass',
        'residue',
        'patch'
    ]
    command_sections = ['declare', 'autogen', 'default']

    def __init__(self):
        self.sections = self.data_sections + self.command_sections

        # data
        self.bond = None
        self.angle = None
        self.dihedral = None
        self.improper = None
        self.cmap = None
        self.nonbond = None
        self.nbfix = None
        self.hbond = None
        self.mass = None
        self.residue = None
        self.patch = None

        # commands
        # a dictionary where we stuff unformatted text data, this aspect of
        # the implementation may change as we add non-charmm formats
        self._init_commands() # sets attr:`self.commands`

    def merge(self, other):
        """Creates a new Toppar object, where rtf/prm objects from `self` are
        given priority over rtf/prm objects from `other`.
        """
        tmp = Toppar()
        # merge data attributes
        for section in self.sections:
            try:
                if section == 'mass':
                    merged = merge_mass(self.mass, other.mass)
                elif section == 'cmap':
                    merged = merge_cmap(self.cmap, other.cmap)
                else:
                    merged = merge_section(getattr(self, section), getattr(other, section))
                setattr(tmp, section, merged)
            except AttributeError:
                pass
        # merge command attributes
        for section in self.sections:
            merged = merge_command(self.commands[section], other.commands[section])
            if merged is not None:
                tmp.commands[section] = merged
        return tmp

    def unique(self):
        """Checks each :attr:`data_section` in the current :class:`Toppar` object
        for redundencies, and removes them.
        """
        for section in self.sections:
            try:
                setattr(self, section, _unique(getattr(self, section)))
            except AttributeError:
                pass

    def __add__(self, other):
        return self.merge(other)

    def _init_commands(self):
        """Initialize all possible command values to `None`."""
        tmp = dict()
        for section in self.sections:
            tmp[section] = None
        self.commands = tmp
