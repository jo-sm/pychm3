from copy import deepcopy

import warnings

def unique(this):
    """Accepts a :class:`list` and returns a new list containing only the unique
    elements, while preserving the initial order.
    """
    if this is None:    # short circuit for Nonetype
        return None
    if this == []:      # short circuit empty lists
        return None
    this = deepcopy(this)
    if len(this) == len(set(this)): # short circuit for already unique lists
        return this
    tmp = []
    dupes = []
    for data in this:
        if data in tmp:
            dupes.append(data)
        else:
            tmp.append(data)
    if dupes:
        warnings.warn("Duplicate data: %r" % dupes)
    return tmp

def merge_section(this, that):
    """Function for merging two :class:`list`s containing :class:`PRM`-like
    objects. This function should not be called on :class:`Mass` or
    :class:`CmapPRM` objects, they have their own specialized logic.
    """
    this = unique(this)
    that = unique(that)
    if this is None:
        return that
    if that is None:
        return this
    if this == that:
        return this
    #
    for data in that:
        if data not in this:
            this.append(data)
    return this

def merge_mass(this, that):
    """Function for merging two :class:`list`s containing :class:`Mass` objects.

    First attempts to naively merge the two lists by simply adding them
    (preserving ordering and indexing). Failing that, the lists are combined,
    the ordering is lost, and new :attr:`id`s are assigned.
    """
    this = unique(this)
    that = unique(that)
    if this is None:
        return that
    if that is None:
        return this
    if this == that:
        return this
    # try merging naively, then check for overlap atom and id
    tmp = this + that
    tmpid = [ x.id for x in tmp ]
    if len(this) + len(that) == len(set(tmp)) and len(this) + len(that) == len(set(tmpid)):
        # naive merge works!
        return sorted(tmp, key=lambda x: x.id)
    # naive merge fails
    tmp = sorted(list(set(tmp)))
    for i in range(len(tmp)):
        tmp[i].id = i+1
    warnings.warn("Original mass id's could not be retained!")
    return tmp

def merge_cmap(this, that):
    """Function for merging two :class:`CmapPRM` objects. It is currently
    stupid.

    If two different CMAP objects are given, a :exec:`CMAP_Exception` is raised.
    """
    this = unique(this)
    that = unique(that)
    if this is None:
        return that
    if that is None:
        return this
    if this == that:
        return this
    raise TypeError("CMAPs are different between toppar")

def merge_command(this, that):
    """Function for merging two command_strings. It is currently stupid.

    If two different strings are given, the first one is merely returned, and a
    warning is thrown.
    """
    if this is None:
        return that
    if that is None:
        return this
    if this == that:
        return this
    warnings.warn("Conflicting command strings:\n\n%s\n\n%s\n\nusing first string" % (this, that))
    return this
